package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.components.FixedJPanel;
import ru.spb.beavers.core.data.StorageModules;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View описания теоретического решения
 */
public class TheoryView extends JPanel {

    private final JPanel theoryPanel = new FixedJPanel();
    private final JButton btnGoDescription = new JButton("Описание");
    private final JButton btnGoInputs = new JButton("Решение");

    private final TheoryViewPresenter presenter = new TheoryViewPresenter();

    public TheoryView() {
        super(null);

        theoryPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        theoryPanel.setSize(850, 400);
        theoryPanel.setLayout( null );

        JScrollPane theoryPanelWrapper = new JScrollPane(theoryPanel);
        theoryPanelWrapper.setSize(850, 400);
        theoryPanelWrapper.setLocation(20, 20);
        this.add(theoryPanelWrapper);

        btnGoDescription.setSize(100, 30);
        btnGoDescription.setLocation(20, 450);
        this.add(btnGoDescription);

        btnGoInputs.setSize(100, 30);
        btnGoInputs.setLocation(770, 450);
        this.add(btnGoInputs);

        initListeners();
    }

    private void initListeners() {
        btnGoDescription.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoDescriptionPressed();
            }
        });

        btnGoInputs.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoInputsPressed();
            }
        });
    }

    public JPanel getTheoryPanel() {
        return theoryPanel;
    }

    @Override
    public void removeAll() {
        theoryPanel.removeAll();
    }

    private class TheoryViewPresenter {

        public void btnGoDescriptionPressed() {
            GUIManager.setActiveView(GUIManager.getDescriptionView());
        }

        public void btnGoInputsPressed() {
            InputView inputView = GUIManager.getInputView();
            StorageModules.getActiveModule().initInputPanel(inputView.inputPanel);
            GUIManager.setActiveView(inputView);
        }
    }
}
